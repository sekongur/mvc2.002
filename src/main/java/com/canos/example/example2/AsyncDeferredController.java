package com.canos.example.example2;

import java.util.concurrent.CompletableFuture;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.context.request.async.DeferredResult;

@Controller
public class AsyncDeferredController {
    private final Logger logger = Logger.getLogger(this.getClass());
    private TaskService taskService;
    
    @Autowired
    public AsyncDeferredController(TaskService taskService) {
        this.taskService = taskService;
    }
    

    @RequestMapping(value = "/deferred", method = RequestMethod.GET)
    @ResponseBody
    public DeferredResult<String> executeSlowTask() {
    	logger.info("Request received");
    	
        DeferredResult<String> deferredResult = new DeferredResult<>();
        CompletableFuture
        	.supplyAsync(taskService::execute)
            .whenCompleteAsync((result, throwable) -> deferredResult.setResult(result));
        
        logger.info("Servlet thread released");
        
        return deferredResult;
    }
    
    
}